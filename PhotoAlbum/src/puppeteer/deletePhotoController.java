package puppeteer;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import maine.userList;
import model.Album;

public class deletePhotoController {
	
	public Stage primaryStage;
	public int flag;
	
	@FXML Button cancel;
	@FXML Button accept;
	
	public void setStuff(Stage primaryStage){
		this.primaryStage=primaryStage;
		this.flag=0;
	}
	
	public void activate(){
		this.flag=1;
		primaryStage.close();
	}
	public void dissuade(){
		primaryStage.close();
	}
}
