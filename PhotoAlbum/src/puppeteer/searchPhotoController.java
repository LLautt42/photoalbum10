package puppeteer;

import model.User;
import model.vainPhoto;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.util.Callback;
import maine.saveAndLoad;
import maine.userList;
import model.Album;
import model.Photo;
import model.Tag;
import model.vainUser;


public class searchPhotoController {
	
	public Stage primaryStage;
	public userList UserList;
	public User u;
	
	@FXML Button search;
	@FXML Button terraForma;
	@FXML ListView<vainPhoto> lview;
	@FXML TextField toDate;
	@FXML TextField fromDate;
	@FXML TextField tagType;
	@FXML TextField tagValue;
	@FXML ImageView imgView;
	
	private ObservableList<vainPhoto> searchResults = FXCollections.observableArrayList();
	
	
	public void setStuff(userList UserList, Stage primaryStage, User u){
		this.UserList=UserList;
		this.primaryStage=primaryStage;
		this.u=u;
	}
	
	public void displayImage(vainPhoto foto) {		
		if(foto != null) {
			imgView.setImage(new Image(foto.getName()));			
		}
		else {
			imgView.setImage(new Image("/viewtiful/Penguins.jpg"));
		}
	}
	
	public void initialize() {
		lview.setCellFactory(new Callback<ListView<vainPhoto>, ListCell<vainPhoto>>() {

            public ListCell<vainPhoto> call(ListView<vainPhoto> param) {
                ListCell<vainPhoto> cell = new ListCell<vainPhoto>() {

                    @Override
                    protected void updateItem(vainPhoto item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                        	textProperty().bind(item.getNameProperty());
                        }
                        else{
                        	textProperty().unbind();
                        	textProperty().set("");
                        }
                    }
                };
                return cell;
            }
        });
	}
	
	@SuppressWarnings({ "unused", "null" })
	public void activateSearch() throws ParseException{
		searchResults.clear();
		String toText;
		String fromText;
		String tagTypeText;
		String tagValueText;
		toText=toDate.getText();
		fromText=fromDate.getText();
		tagTypeText=tagType.getText();
		tagValueText=tagValue.getText();
		
		String searchDate1="";
		String searchDate2="";
		
		List<Photo> photos;
		
		//Date search
		if(!toText.equals("") && !fromText.equals("") && tagTypeText.equals("") && tagValueText.equals("")){
			
			//Scroll through albums
			//Scroll through photos in the albums
			String[] blockFirst=fromText.split("/");
			String[] blockSecond=toText.split("/");
			if(blockFirst.length!=3 || blockSecond.length!=3){
				System.out.println("NO");
				Alert alert = new Alert(AlertType.WARNING);
	            alert.setTitle("One category of search at a time, please");
	            alert.setContentText("Please put something in either the date range OR the tags for the search.");
	            alert.showAndWait();
			}
			
			else{
				String year1=blockFirst[2];
				String month1=blockFirst[1];
				String day1=blockFirst[0];
				
				String year2=blockSecond[2];
				String month2=blockSecond[1];
				String day2=blockSecond[0];
				
				searchDate1=day1+"-"+month1+"-"+year1;
				searchDate2=day2+"-"+month2+"-"+year2;
				
			}
			if(searchDate1.length()!=10 || searchDate2.length()!=10){
				Alert alert = new Alert(AlertType.WARNING);
	            alert.setTitle("One category of search at a time, please");
	            alert.setContentText("Please put something in either the date range OR the tags for the search.");
	            alert.showAndWait();
			}
			else{
				
				List<Album>albums=u.getAllAlbums();
				List<vainPhoto> dispList = null;
				for(Album al: albums){
					List<Photo> pics=al.getPhotos();
					for(Photo ph: pics){
						if(ph!=null){
							SimpleDateFormat sdf=new SimpleDateFormat("MM-dd-yyyy");
							Calendar cal=ph.getDateTime();
							
							Date d=cal.getTime();
							
							//String sting=d.toString();
							
							String sting=sdf.format(d);
							Date d1=sdf.parse(sting);
							
							Date d2=sdf.parse(searchDate1);
							Date d3=sdf.parse(searchDate2);
							
							System.out.println(d1.toString());
							System.out.println(d2.toString());
							System.out.println(d3.toString());
							if(d2.after(d3)){
								Alert alert = new Alert(AlertType.WARNING);
					            alert.setTitle("Improperly formatted date string");
					            alert.setContentText("The current date is mis-formatted.  Try again");
					            alert.showAndWait();
					            return;
							}
							
							if(!(d1.before(d2)) && !(d1.after(d3))){
								vainPhoto vp=new vainPhoto(ph.getName());
								searchResults.add(vp);
							}
							
							//Date date2=new Date(searchDate1);
							//Date date3=new Date(searchDate2);
							
							/*Date date1=new Date(q);
							Date date2=new Date(searchDate1);
							Date date3=new Date(searchDate2);
							
							
							if(!(date1.before(date2) && date1.after(date3))){
								vainPhoto vp=new vainPhoto(ph.getName());
								dispList.add(vp);
							}*/
							
							
						}
					}
				}
				
				
			}
			if(!searchResults.isEmpty()) {
				
				lview.setItems(searchResults);
				
			}
			//searchResults=null;
			
			
		}
		//Tag Search
		else if(toText.equals("") && fromText.equals("") && !tagTypeText.equals("") && !tagValueText.equals("")){
			
			List<Album>albums=u.getAllAlbums();
			List<vainPhoto> dispList = null;
			for(Album al: albums){
				List<Photo> pics=al.getPhotos();
				System.out.println("Checking album: " + al.getName());
				for(Photo ph: pics){
					if(ph!=null){
						System.out.println("Checking photo: " + ph.getName());
						for(Tag ta: ph.getTags()){
							
							if(ta.getType().equals(tagTypeText) && ta.getValue().equals(tagValueText)){
								//System.out.println("Found matching tag!");
								vainPhoto vp=new vainPhoto(ph.getName());
								//dispList.add(vp);	
								searchResults.add(vp);
							}
						}
					}
				}
			}
			
			/*
			if(dispList!=null){
				lview=(ListView<vainPhoto>)dispList;
			}*/
			
			if(!searchResults.isEmpty()) {
				
				lview.setItems(searchResults);
				
			}
			//searchResults=null;
			
		}
		else{
			Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("One category of search at a time, please");
            alert.setContentText("Please put something in either the date range OR the tags for the search.");
            alert.showAndWait();
		}
	}
	
	public void shipOut(){
		List<vainPhoto> searchList=lview.getItems();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:ms");
		Calendar cal = Calendar.getInstance();
		
		String q="Album Created on: " + dateFormat.format(cal.getTime()); 
		u.addAlbum(q);

		for (vainPhoto searchResult: searchResults) {
			u.getAlbum(q).addPhoto(new Photo(searchResult.getName(), "This photo was added from a search!"));
		}
		/*
		List<Album>albums=u.getAllAlbums();
		
		for(Album al: albums){
			List<Photo> pics=al.getPhotos();
			for(Photo ph: pics){
				if(ph!=null){
					vainPhoto vp=new vainPhoto(ph.getName());
					if(searchList.contains(vp)){
						ally.addPhoto(ph);
					}
				}
			}
		}*/
		//u.addAlbum();
		return;
		
	}
	@FXML
	public void exitApplication(ActionEvent event) {
	   Platform.exit();
	}
	
	public void stop(){
			saveAndLoad.saveGame(UserList);
			System.out.println("Game saved.");
			//Platform.exit();
	}
	
	

}
