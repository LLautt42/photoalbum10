package puppeteer;

import java.awt.Button;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import maine.saveAndLoad;
import maine.userList;
import model.Album;
import model.User;

/**
 * Main controller for the renaming of albums window.
 * 
 * @author Logan
 * 
 * @param UserList	The global User List
 * @param primaryStage	The current stage of the program
 * @param album	The current album to be edited.
 * @param user	The user that owns the album
 * 
 */
public class renameAlbumController {
	public userList UserList;
	public Stage primaryStage;
	public Album album;
	public User user;
	
	@FXML TextField albumName;
	@FXML Button rename;
	
	/**
	 * Constructor.  Self Explanatory.
	 * @param UserList 
	 * @param primaryStage
	 * @param album
	 * @param user
	 */
	
	public void setStuff(userList UserList, Stage primaryStage, Album album, User user){

		this.UserList=UserList;
		this.primaryStage=primaryStage;
		this.album=album;
		this.user=user;
	}
	/**
	 * @author Logan
	 * 
	 * The following function activates the button to edit the album.
	 * 
	 * 
	 */
	public void activate(){
		
		
		String input= albumName.getText();
		if(input.equals("")){
			Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("NO INPUT");
            alert.setContentText("YOU MESSED UP!");
            alert.showAndWait();
		}
		else if(user.getAlbum(input)!=null){
			Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("THIS NAME WAS ALREADY USED");
            alert.setContentText("PICK ANOTHER ONE!");
            alert.showAndWait();
		}
		else{
			//System.out.println(album.getName());
			//album.setName(input);
			user.renameAlbum(album.getName(), input);
			albumListController.refreshObs();
			//saveAndLoad.saveGame(UserList);
			primaryStage.close();
		}
			
	}
}
