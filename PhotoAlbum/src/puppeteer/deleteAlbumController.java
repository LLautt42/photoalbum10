package puppeteer;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import maine.userList;
import model.Album;
import model.User;

public class deleteAlbumController {
	
	public userList UserList;
	public Stage primaryStage;
	public User user;
	
	@FXML Button yeah;
	
	public void setStuff(userList UserList, Stage primaryStage, User user){

		this.UserList=UserList;
		this.primaryStage=primaryStage;
		this.user=user;
	}
	
	public boolean findPhoto(String song){
		if(user.getAlbum(song)!=null){
			return true;
		}
		return false;
	}
	
	public void activate(){
		
		
	}
}
