package puppeteer;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import model.User;
import maine.photoalbum;
import maine.userList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class loginController{
	

	
	@FXML TextField userfield;
	@FXML Button login;

	public userList UserList;
	public Stage primaryStage;
	
	public loginController(){}
	
	
	/**
	 * @author Logan Lautt
	 * The following function sets the userList ad the primaryStage of the class for when it has to launch additional FXMLs
	 *
	 * @param	UserList	the main userlist of the program
	 * @param primaryStage	the primaryStage for launching FXMLs
	 */
	public void setStuff(userList UserList, Stage primaryStage){

		this.UserList=UserList;
		this.primaryStage=primaryStage;
	}
	
	/**
	 * @author Logan Lautt
	 * The following function searches a user list to see if the person is extant.
	 *
	 * @param	input	the person's input to be checked as a name
	 * @return	1 if the user is admin, 0 if the user is extant and not admin, -1 otherwise
	 */
	//Checks to see if a user has indeed signed up.
	public int seekPerson(String input){
		if(input.equals("admin")){
			return 1;
		}
		else if(UserList.findUser(input)){
			return 0;
		}
		else{
			return -1;
		}
	}
	
	/**
	 * @author Logan Lautt
	 * The following function launches the admin screen if the user is admin, the corresponding user screen if the user is
	 * extant on the list, and notifies the user afterward.
	 *
	 * @throws IOException
	 */
	public void judicate() throws IOException{
		if(seekPerson(userfield.getText())==1){
			//Launch adminSS
			FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(getClass().getResource("/viewtiful/AdminSS.fxml"));
	        GridPane foundation = (GridPane)loader.load();
	        
	        Stage adminStage = new Stage();
	        adminStage.setTitle("ADMIN SCREEN");
	        adminStage.initOwner(primaryStage);
	        
	        adminSSController acontroller=loader.getController();
	        acontroller.setStuff(UserList, primaryStage);
	         
	         
	        Scene albumScene= new Scene(foundation);
	        primaryStage.setScene(albumScene);
	        primaryStage.show();
			
		
			}
			else if(seekPerson(userfield.getText())==0){
				//Launch albumList
				//Find user
				String temp=userfield.getText();
				User tempUser=UserList.findUserName(temp);
				
				FXMLLoader loader2 = new FXMLLoader();
		        loader2.setLocation(getClass().getResource("/viewtiful/AlbumList.fxml"));
		        GridPane foundation = (GridPane)loader2.load();
		        
		        Stage userStage = new Stage();
		        userStage.setTitle("USER SCREEN");
		        userStage.initOwner(primaryStage);
		        
		        albumListController albumcontroller=loader2.getController();
		        albumcontroller.setStuff(UserList, primaryStage, tempUser);
		         
		         
		        Scene albumScene= new Scene(foundation);
		        primaryStage.setScene(albumScene);
		        primaryStage.show();
			}
			else{
				Alert alert = new Alert(AlertType.WARNING);
	            alert.setTitle("YOU DIDN'T PUT AN EXTANT NAME IN, DOOFUS");
	            alert.setContentText("ACCESS DENIED!");
	            alert.showAndWait();
				
			}
	}
	
	
	

}
