package puppeteer;


import java.awt.Button;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import maine.saveAndLoad;
import maine.userList;
import model.Album;
import model.Photo;
import model.User;
import model.vainAlbum;
import model.vainUser;

/**
 * Allows the user to see their albums, and allows editing of said albums.
 * @author Logan Lautt
 *
 *@param UserList	The master list of users
 *@param primaryStage	The current stage of the program
 *@param user	The user that is to be audited.
 */
public class albumListController {
	
	public userList UserList;
	public Stage primaryStage;
	public static User user;
	//public userList tempUserList;
	
	private static ObservableList<vainAlbum>  obsAlbumList = FXCollections.observableArrayList();
	private static vainAlbum obsAlbumName;
	
	@FXML Label name;
	@FXML
		public ListView<vainAlbum> lview;
	@FXML Label number;
	@FXML Label oldest;
	@FXML Label date;
	@FXML Button open;
	@FXML Button rename;
	@FXML Button delete;
	@FXML Button createAlbum;
	@FXML Button searchPhotos;
	@FXML Button logout;
	@FXML TextField Username;
	

	
	public void setStuff(userList UserList, Stage primaryStage, User user){

		this.UserList=UserList;
		this.primaryStage=primaryStage;
		this.user=user;
		//this.tempUserList=UserList;
		refreshObs();
	}
	
	public albumListController(){
		
	}
	/**
	 * @author Logan Lautt
	 * returns the observable list
	 * @return
	 */
	 public ObservableList<vainAlbum> getObsAlbumList() {
	        return obsAlbumList;
	 }
	
	 /**
	  * @author Logan Lautt
	  * Refreshes the listview with new data.
	  */
	public static void refreshObs(){
		obsAlbumList.clear();
		
		for (Album album: user.getAllAlbums()) {

			obsAlbumName = new vainAlbum(album.getName());
			obsAlbumList.add(obsAlbumName);
		}
	}
	
	
	
	/**
	 * @author Angeline Tamayo
	 * 
	 * Updates the ListView with the observable list of usernames 
	 */
	@FXML
    private void initialize(){
		
		//refreshObs();
		
		// Add observable list of usernames to the ListView
		lview.setItems(obsAlbumList);
		
		lview.setCellFactory(new Callback<ListView<vainAlbum>, ListCell<vainAlbum>>() {

            public ListCell<vainAlbum> call(ListView<vainAlbum> param) {
                ListCell<vainAlbum> cell = new ListCell<vainAlbum>() {

                    @Override
                    protected void updateItem(vainAlbum item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                        	textProperty().bind(item.getNameProperty());
                        }
                        else{
                        	textProperty().unbind();
                        	textProperty().set("");
                        }
                    }
                };
                return cell;
            }
        });
		
		lview.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> displayProps(newValue));
		//refreshObs();
    }
	
	public void displayProps(vainAlbum vAlbum){
		if(vAlbum!=null){
			name.setText(vAlbum.getName());
		}
		else{
			name.setText(" ");
			return;
		}
		
		
		if(user.getAlbum(vAlbum.getName())==null){
			return;
		}
		
		int count=0;
		if(user.getAlbum(vAlbum.getName()) != null){
			if(user.getAlbum(vAlbum.getName()).getPhotos() != null){
				for(Photo phot: user.getAlbum(vAlbum.getName()).getPhotos()){
					count++;
				}
			}
			
		}
		
		number.setText(Integer.toString(count));
		
		if(user.getAlbum(vAlbum.getName()).getPhotos().isEmpty()){
			oldest.setText("No Photos");
			date.setText("No Photos");
			return;
		}
		
		oldest.setText(" ");
		date.setText(" ");
		
		Calendar geezer=findOldest(user.getAlbum(vAlbum.getName()));
		Calendar babe=findNewest(user.getAlbum(vAlbum.getName()));
		
		if(geezer==null){
			oldest.setText(" ");
		}
		else if(babe==null){
			date.setText(" ");
		}
		else{
			//System.out.println("HEY");
			
			Date d=geezer.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy");
			
			
			
			String old=geezer.getTime().toString();
			oldest.setText(old);
			
			//System.out.println(geezer.getTime());
			String born=babe.getTime().toString();
			String combo=old + " to \n" + born;
			date.setText(combo);
		}
	}
	
	public Calendar findOldest(Album a){
		Calendar temp = null;
		int semper=0;
		if(a.getPhotos()==null){
			return null;
		}
		for(Photo phot: a.getPhotos()){
			if(semper==0){
				temp=phot.getDateTime();
				
				semper=99;
			}
			else{
				if(phot.getDateTime().compareTo(temp)>0){
					continue;
				}
				else if(phot.getDateTime().compareTo(temp)<0){
					temp=phot.getDateTime();
				}
				else{
					continue;
				}
			}
		}
		
		return temp;
	}
	public Calendar findNewest(Album a){
		Calendar temp = null;
		int semper=0;
		for(Photo phot: a.getPhotos()){
			if(semper==0){
				temp=phot.getDateTime();
				semper=99;
			}
			else{
				if(phot.getDateTime().compareTo(temp)<0){
					continue;
				}
				else if(phot.getDateTime().compareTo(temp)>0){
					temp=phot.getDateTime();
				}
				else{
					continue;
				}
			}
		}
		
		return temp;
	}
	

	/**
	 * Allows for the launching of the photolist.
	 * @throws IOException
	 */
	public void openGov() throws IOException{
	//refreshObs();	
	//Check for listview selection
		//Launch photolist
		
		vainAlbum vAlbum=lview.getSelectionModel().getSelectedItem();
		if(vAlbum==null){
			return;
		}
		//refreshObs();
		Album ally=null;
		String target=vAlbum.getName();
		for(Album a: user.getAllAlbums()){
			if(a.getName().equals(target)){
				ally=a;
				break;
			}
		}
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/viewtiful/PhotoList.fxml"));
        GridPane Root=(GridPane)loader.load();
        
        photoListController photocontroller=loader.getController();
        
        
        Stage stage=new Stage();
        Scene photoScene= new Scene(Root);
        stage.setScene(photoScene);
        photocontroller.setStuff(UserList, stage, ally, user);
        
        
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		      public void handle(WindowEvent we) {
		          System.out.println("Stage is closing");
		          saveAndLoad.saveGame(UserList);
		          System.out.println("Player saved the game!");
		          refreshObs();
		      }
		  }); 
        
        stage.show();
		
	}
	/**
	 * Launches the window to edit albums.
	 * @throws IOException
	 */
	public void renameGov() throws IOException{
		
		vainAlbum vAlbum=lview.getSelectionModel().getSelectedItem();
		if(vAlbum==null){
			return;
		}
		Album ally=user.getAlbum(vAlbum.getName());
		
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/viewtiful/RenameAlbumDialog.fxml"));
        AnchorPane Root=(AnchorPane)loader.load();
        
        renameAlbumController renamecontroller=loader.getController();
        
        Stage stage=new Stage();
        Scene renameScene= new Scene(Root);
        stage.setScene(renameScene);
        renamecontroller.setStuff(UserList, stage, ally, user);
        stage.show();
        //refreshObs();
        
        
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		      public void handle(WindowEvent we) {
		          System.out.println("Stage is closing");
		          saveAndLoad.saveGame(UserList);
		          System.out.println("Player saved the game!");
		          refreshObs();
		      }
		  });
        
        
        saveAndLoad.saveGame(UserList);
		
		
	}
	/**
	 * Launches the window to delete albums.
	 * 
	 */
	public void deleteGov(){
		
		vainAlbum vAlbum=lview.getSelectionModel().getSelectedItem();
		if(vAlbum==null){
			return;
		}
		 Alert alert = new Alert(AlertType.CONFIRMATION);
   		alert.setTitle("DELETE ALBUM");
   		alert.setHeaderText("Confirm Album Deletion");
   		alert.setContentText("Are you sure you want to delete this Album?");

   		Optional<ButtonType> result = alert.showAndWait();
   		if (result.get() == ButtonType.OK){
   			
   			user.deleteAlbum(vAlbum.getName());    			
   			    			
   		}
   		refreshObs();
   		saveAndLoad.saveGame(UserList);
   		
   		for(Album album: user.getAllAlbums()){
   			System.out.println(album.getName());
   			//user.deleteAlbum(album.getName());
   		}
		
	}
	
	/**
	 * Launches the window to create albums.
	 * @throws IOException
	 */
	public void createGov() throws IOException{
		
		//refreshObs();
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/viewtiful/NewAlbumDialog.fxml"));
        GridPane Root=(GridPane)loader.load();
        
        newAlbumController albumcontroller=loader.getController();
        
        Stage stage=new Stage();
        Scene albumScene= new Scene(Root);
        stage.setScene(albumScene);
        albumcontroller.setStuff(UserList, stage, user);
        stage.show();
        refreshObs();
        saveAndLoad.saveGame(UserList);
		
	}
	
	/**
	 * Launches the window to search albums.
	 * @throws IOException
	 */
	public void searchGov() throws IOException{
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/viewtiful/SearchPhotos.fxml"));
        GridPane Root=(GridPane)loader.load();
        
        searchPhotoController searchcontroller=loader.getController();
        
        Stage stage=new Stage();
        Scene searchScene= new Scene(Root);
        stage.setScene(searchScene);
        searchcontroller.setStuff(UserList, primaryStage, user);
        
        


		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		      public void handle(WindowEvent we) {
		          System.out.println("Stage is closing");
		          saveAndLoad.saveGame(UserList);
		          System.out.println("Player saved the game!");
		          refreshObs();
		      }
		  }); 


        
        
        stage.show();
        saveAndLoad.saveGame(UserList);
	}
	
	/**
	 * Launches the window to logout.
	 * @throws IOException
	 */
	public void logoutGov() throws IOException{
		//refreshObs();
		saveAndLoad.saveGame(UserList);
		//Commits changes.
		//UserList=tempUserList;
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/viewtiful/Login.fxml"));
        Pane loginRoot=(Pane)loader.load();
        
        loginController lcontroller=loader.getController();
        lcontroller.setStuff(UserList, primaryStage);
        
        
        Scene loginScene= new Scene(loginRoot);
        primaryStage.setScene(loginScene);
        primaryStage.show();
	}
}
