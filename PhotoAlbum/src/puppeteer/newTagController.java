package puppeteer;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import maine.saveAndLoad;
import maine.userList;
import model.Photo;
import model.Tag;

public class newTagController {

	public userList UserList;
	public Stage tagStage;
	public Photo photo;
	
	@FXML Button addTag;
	@FXML TextField tagType;
	@FXML TextField tagValue;
	boolean tagAlreadyExists = false;
	
	public void setStuff(userList UserList, Stage tagStage, Photo photo){

		this.UserList=UserList;
		this.tagStage=tagStage;
		this.photo=photo;
		
		tagType.setPromptText("The type of tag (e.g. \"location\". \"color\")");
		tagValue.setPromptText("The value of the tag (e.g. \"New Brunswick\". \"blue\")");
	}
	
	public void activate() {
		
		String type = tagType.getText();
		String val = tagValue.getText();
		
		// Check if tag already exists
		for (Tag t: photo.getTags()) {
			Alert alert = new Alert(AlertType.WARNING);
			if (type.equals(t.getType()) && val.equals(t.getValue())) {
				
            	alert.initOwner(tagStage);
            	alert.setTitle("TAG ALREADY EXISTS");     
            	alert.setContentText("A tag with that type and value already exists. Please enter a new type and value!");
            	alert.showAndWait();
            	
            	tagAlreadyExists = true;
			}
		}
		
		if (!tagAlreadyExists) 
			photo.addTag(type, val);
		
		saveAndLoad.saveGame(UserList);
		tagStage.close();
		
	}
}
