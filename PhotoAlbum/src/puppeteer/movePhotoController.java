package puppeteer;
import java.io.IOException;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import model.Album;
import model.Photo;
import model.User;
import model.vainAlbum;
import model.vainPhoto;
import maine.saveAndLoad;
import maine.userList;


public class movePhotoController {
	@FXML 
		public ListView<String> lview;
	@FXML Button move;
	
	public userList UserList;
	public Stage primaryStage;
	public User u;
	public Album album;
	public Photo photo;
	
	public static ObservableList<String>  obsAlbumList = FXCollections.observableArrayList();
	public static vainAlbum obsAlbumName;
	
	public void setStuff(userList UserList, Stage primaryStage, User u, Album album, Photo photo){
		this.UserList=UserList;
		this.primaryStage=primaryStage;
		this.u=u;
		this.album=album;
		this.photo=photo;
		listAlbums();
		
	}
	
	public void listAlbums(){
		obsAlbumList.clear();
		
		for (Album album: u.getAllAlbums()) {

			obsAlbumName = new vainAlbum(album.getName());
			obsAlbumList.add(obsAlbumName.getName());
			System.out.println(obsAlbumName.getName());
		}
		lview.setItems(obsAlbumList);
		
	}
	private void initialize(){
		lview.setItems(obsAlbumList);	
	}
	public void smash(){
		String topic=lview.getSelectionModel().getSelectedItem();
		Album al=u.getAlbum(topic);
		Album oldAl=album;
		if(al.getName().equals(oldAl.getName())){
			return;
		}
		String target=photo.getName();
		if(al.getPhotos()==null){
			//Add immediately.  Close.
		}
		
		for(Photo ph: al.getPhotos()){
			if(ph.getName().equals(target)){
				return;
			}
		}
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
   		alert.setTitle("MOVE PICTURE");
   		alert.setHeaderText("Confirm Picture Movement");
   		alert.setContentText("Are you sure you want to move this picture?");

   		Optional<ButtonType> result = alert.showAndWait();
   		if (result.get() == ButtonType.OK){
   			
   			al.addPhoto(photo);
   			oldAl.deletePhoto(target);
   			saveAndLoad.saveGame(UserList);
   			primaryStage.close();    			
   			    			
   		}
   		
		
		
	}
	
	

}

/*
 * 	public void moveGov() throws IOException{
		  vainPhoto vp=lview.getSelectionModel().getSelectedItem();
	        if(vp==null){
	        	return;
	        }
	        Photo ph=null;
	        for(Photo pho: album.getPhotos()){
	        	if(pho.getName().contains(vp.getName())){
	        		ph=pho;
	        		break;
	        	}
	        }
		
		
		FXMLLoader loader2 = new FXMLLoader();
        loader2.setLocation(getClass().getResource("/viewtiful/MovePhotoDialog.fxml"));
        AnchorPane moveRoot=(AnchorPane)loader2.load();
        
        movePhotoController mcontroller=loader2.getController();
       
        
        Stage stage=new Stage();
        Scene moveScene= new Scene(moveRoot);
        
        
        
        stage.setScene(moveScene);
        mcontroller.setStuff(UserList, stage, u, album, ph);
        
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		      public void handle(WindowEvent we) {
		          System.out.println("Stage is closing");
		          saveAndLoad.saveGame(UserList);
		          System.out.println("Player saved the game!");
		          refreshObsList();
		      }
		  }); 
        
        stage.show();
        refreshObsList();
        //saveAndLoad.saveGame(UserList);
	}
 * 
 * 
 * 
 * 
 * 
 */
