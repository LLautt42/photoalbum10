package puppeteer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Optional;

import javax.imageio.ImageIO;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import maine.saveAndLoad;
import maine.userList;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;
import model.vainPhoto;
import model.vainTag;

public class photoListController {
	public userList UserList;
	public Stage primaryStage;
	public Album album = new Album(null);
	public User u;
	// Month array for photo dates
	String months[] = {
			"Jan", "Feb", "Mar", "Apr",
		    "May", "Jun", "Jul", "Aug",
		    "Sep", "Oct", "Nov", "Dec"};
	
	
	@FXML ImageView imgView; 
	@FXML Button addPhoto;
	@FXML Button editCaption;
	@FXML Button addTag;
	@FXML Button delTag;
	@FXML Button movePhoto;
	@FXML Button delPhoto;
	//@FXML Button back;
	@FXML Button logout;
	@FXML ListView<vainPhoto> lview;
	@FXML Label tag1;
	@FXML Label tag2;
	@FXML Label dateTime;
	@FXML TextArea caption;
	@FXML TableView<vainTag> tview;
	@FXML TableColumn<vainTag, String> tagTypeColumn;
	@FXML TableColumn<vainTag, String> tagValueColumn;
	
    /**
     * All photo names of an album as an observable list
     */
	private ObservableList<vainPhoto>  obsPhotoList = FXCollections.observableArrayList();
	private vainPhoto obsPhotoName;
	
	/**
	 * All tags of a photo as an observable list
	 */
	private ObservableList<vainTag> obsTagList = FXCollections.observableArrayList();
	private vainTag obsTag;
	
	public void setStuff(userList UserList, Stage primaryStage, Album album, User u){

		this.UserList=UserList;
		this.primaryStage=primaryStage;
		this.album=album;
		this.u=u;
		
		refreshObsList();
	}
	
	public void initialize(){
		
		refreshObsList();
		
		// Add observable list of photo names to the ListView
		lview.setItems(obsPhotoList);
		
		// Initialize the list view with the list of photo names
		lview.setCellFactory(new Callback<ListView<vainPhoto>, ListCell<vainPhoto>>() {

            public ListCell<vainPhoto> call(ListView<vainPhoto> param) {
                ListCell<vainPhoto> cell = new ListCell<vainPhoto>() {

                    @Override
                    protected void updateItem(vainPhoto item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                        	textProperty().bind(item.getNameProperty());
                        }
                        else{
                        	textProperty().unbind();
                        	textProperty().set("");
                        }
                    }
                };
                return cell;
            }
        });
		
        tview.setItems(obsTagList);
        
        // Initialize the tag table with the two columns        
        tagTypeColumn.setCellValueFactory(t -> t.getValue().getTypeProperty());
        tagValueColumn.setCellValueFactory(t -> t.getValue().getValueProperty());
		
		refreshObsList();
		lview.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> displayImage(newValue));
	}
	
	public void displayImage(vainPhoto foto) {		
		
		
		if(foto != null) {

			imgView.setImage(new Image("photos/" + foto.getName()));
			caption.setText(foto.getCaption());
			refreshTags();
	        vainPhoto vPhoto = lview.getSelectionModel().getSelectedItem();
	        for (Photo pic: album.getPhotos()){ //lol dont judge me
	        	if (pic.getName().equals("src\\photos/" + vPhoto.getName())) {
	        		
	        		dateTime.setText("Taken on "
	        				+ months[pic.getDateTime().get(Calendar.MONTH)] + " "
	        				+ pic.getDateTime().get(Calendar.DATE) + " "
	        				+ pic.getDateTime().get(Calendar.YEAR) + " at "
	        				+ pic.getDateTime().get(Calendar.HOUR) + ":"
	        				+ pic.getDateTime().get(Calendar.MINUTE)
	        				+ ((pic.getDateTime().get(Calendar.AM_PM)) == 0 ? "AM" : "PM") );
	        	}
	        }
		}
	}
	
	public void refreshObsList() {
		
		obsPhotoList.clear();

		for (Photo photo: album.getPhotos()) {
			
			obsPhotoName = new vainPhoto(photo.getName().substring(11,photo.getName().length()));
			obsPhotoName.setCaption(photo.getCaption());
			obsPhotoList.add(obsPhotoName);
		}
	}
	
	public void refreshTags() {
		if (obsTagList != null){
		
			obsTagList.clear();
			vainPhoto victim = lview.getSelectionModel().getSelectedItem();
		
			for (Photo photo: album.getPhotos()) {
				if (photo.getName().equals("src\\photos/" + victim.getName())) {
					for (Tag t: photo.getTags()) {
					
						obsTag = new vainTag(t.getType(), t.getValue());
						obsTagList.add(obsTag);
					}
				}
			}
		}
	}

	public void addPhotoGov() throws IOException{
        
		FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "Image Files (*.jpg, *.jpeg, *.png, *.bmp, *.tif)", "*.jpg", "*.jpeg", "*.png", "*.bmp", "*.tif");
        fileChooser.getExtensionFilters().add(extFilter);
        
        // Show open file dialog
        Stage loadPhoto = new Stage();
        File img = fileChooser.showOpenDialog(loadPhoto);
        
        if (img != null) {

        	File directory = new File("src/photos");
        	
        	// Create photo directory if it doesn't exist
        	if (!directory.exists()) {
        		directory.mkdir();
        	}
        		
    	try {  	        
    		// Save photo to photo directory
    		String imgPath = directory.getPath() + "/" + img.getName();
  	        
  	        // Extract file extension
  	        String imgExt = imgPath.substring((imgPath.length() - 3), imgPath.length());
  	        
  	        BufferedImage bimg = ImageIO.read(img);
  	        File bimgFile = new File(imgPath);
  	        ImageIO.write(bimg, imgExt, bimgFile);
  	        Photo savedPhoto = new Photo(imgPath, "I am a caption. ^._.^ Change me right from this box,\nthen click the button below!");
  	        
  	    	// Add photo to album
  	    	album.addPhoto(savedPhoto);
  	      }
    	catch(IOException e){
  	          e.printStackTrace();
  	      }
    	
    	saveAndLoad.saveGame(UserList);
    	refreshObsList();
    	
        }		
	}
	
	public void editCaptionGov() throws IOException{
		String cap = caption.getText();
        vainPhoto vPhoto = lview.getSelectionModel().getSelectedItem();
        for (Photo pic: album.getPhotos()){
        	if (pic.getName().equals("src\\photos/" + vPhoto.getName())) {
        		pic.setCaption(cap);
        	}
        }
        
        saveAndLoad.saveGame(UserList);
        refreshObsList();
	}
	
	public void addTagGov() throws IOException{
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/viewtiful/NewTagDialog.fxml"));
        AnchorPane foundation = (AnchorPane)loader.load();
        
        vainPhoto vPhoto = lview.getSelectionModel().getSelectedItem();
        for (Photo pic: album.getPhotos()){
        	if (pic.getName().equals("src\\photos/" + vPhoto.getName())) {
                newTagController tagcontroller=loader.getController();
                Stage tagStage = new Stage();
                tagStage.setTitle("ADD A TAG");
                tagStage.initOwner(primaryStage);
                tagcontroller.setStuff(UserList, tagStage, pic);
                Scene tagScene= new Scene(foundation);
                tagStage.setScene(tagScene);
                tagStage.showAndWait();
        	}
        }
        
        refreshTags();
        
	}
	
	public void delPhotoGov() throws IOException{

		vainPhoto victim = lview.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(AlertType.CONFIRMATION);
  		alert.setTitle("DELETE PHOTO");
  		alert.setHeaderText("Confirm Photo Deletion");
  		alert.setContentText("Are you sure you want to delete this photo?");

  		Optional<ButtonType> result = alert.showAndWait();
  		if (result.get() == ButtonType.OK){
  			
  			String victimPath = "src\\photos/" + victim.getName();
  			System.out.println(victimPath);
  			album.deletePhoto(victimPath);    			
  			    			
  		}
  		refreshObsList();
  		saveAndLoad.saveGame(UserList);
		
	}
	
	public void delTagGov() throws IOException{
		
		vainTag vTag = tview.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(AlertType.CONFIRMATION);
  		alert.setTitle("DELETE TAG");
  		alert.setHeaderText("Confirm Tag Deletion");
  		alert.setContentText("Are you sure you want to delete this tag?");

  		Optional<ButtonType> result = alert.showAndWait();
  		if (result.get() == ButtonType.OK){
  			
  			vainPhoto vPhoto = lview.getSelectionModel().getSelectedItem();
  			for (Photo p: album.getPhotos()) {
  				if (p.getName().equals("src\\photos/" + vPhoto.getName())) {
  					p.deleteTag(vTag.getType(), vTag.getValue());
  				}	
  			}  						
  		}
  		refreshObsList();
  		refreshTags();
  		saveAndLoad.saveGame(UserList);
	}
	/*
	public void backGov() throws IOException{
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/viewtiful/albumListController.fxml"));
        AnchorPane foundation = (AnchorPane)loader.load();
        
        Stage userStage = new Stage();
        userStage.setTitle("USER SCREEN");
        userStage.initOwner(primaryStage);
        
        albumListController albumcontroller=loader.getController();
        //lcontroller.setStuff(uList, primaryStage);
         
        
        Stage stage=new Stage();
        Scene albumScene= new Scene(foundation);
        stage.setScene(albumScene);
        stage.show();
		
	}*/
	
	public void moveGov() throws IOException{
		  vainPhoto vp=lview.getSelectionModel().getSelectedItem();
	        if(vp==null){
	        	return;
	        }
	        Photo ph=null;
	        for(Photo pho: album.getPhotos()){
	        	if(pho.getName().contains(vp.getName())){
	        		ph=pho;
	        		break;
	        	}
	        }
		
		
		FXMLLoader loader2 = new FXMLLoader();
        loader2.setLocation(getClass().getResource("/viewtiful/MovePhotoDialog.fxml"));
        AnchorPane moveRoot=(AnchorPane)loader2.load();
        
        movePhotoController mcontroller=loader2.getController();
       
        
        Stage stage=new Stage();
        Scene moveScene= new Scene(moveRoot);
        
        
        
        stage.setScene(moveScene);
        mcontroller.setStuff(UserList, stage, u, album, ph);
        
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		      public void handle(WindowEvent we) {
		          System.out.println("Stage is closing");
		          saveAndLoad.saveGame(UserList);
		          System.out.println("Player saved the game!");
		          refreshObsList();
		      }
		  }); 
        
        stage.show();
        refreshObsList();
        //saveAndLoad.saveGame(UserList);
	}
	
	public void logoutGov() throws IOException{
		
		saveAndLoad.saveGame(UserList);
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/viewtiful/Login.fxml"));
        Pane loginRoot=(Pane)loader.load();
        
        loginController lcontroller=loader.getController();
        lcontroller.setStuff(UserList, primaryStage);

        Scene loginScene= new Scene(loginRoot);
        primaryStage.setScene(loginScene);
        primaryStage.show();
		
	}
}
