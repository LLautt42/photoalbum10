package puppeteer;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import maine.saveAndLoad;
import maine.userList;
import model.Album;
import model.User;

public class newAlbumController {

	public userList UserList;
	public Stage primaryStage;
	public User user;
	
	@FXML TextField albumName;
	@FXML Button create;
	
	public void setStuff(userList UserList, Stage primaryStage, User user){

		this.UserList=UserList;
		this.primaryStage=primaryStage;
		this.user=user;
	}
	
	public void activate(){
		String input=albumName.getText();
		
		if(user.getAlbum(input)!=null){
			Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("THIS NAME WAS ALREADY USED");
            alert.setContentText("PICK ANOTHER ONE!");
            alert.showAndWait();
		}
		else{
			user.addAlbum(input);
			saveAndLoad.saveGame(UserList);
			albumListController.refreshObs();
			primaryStage.close();
		}
	}
	
}
