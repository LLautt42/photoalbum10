package puppeteer;

import java.io.IOException;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.User;
import model.vainUser;
import maine.saveAndLoad;
import maine.userList;

/**
 * Admin Sub-System controller. Allows the admin can view a list of users,
 * create a new user, and delete an existing user.
 * 
 * @author Angeline Tamayo
 *
 */
public class adminSSController {
	@FXML
	private ListView<vainUser> listView;
	@FXML
		Button createUser;
	@FXML
		Button deleteUser;
	@FXML
		Button logOut;
	@FXML
		Label adminMenu;
	
	private userList UserList = new userList();
	public Stage primaryStage;
	
    /**
     * All usernames as an observable list
     */
	private ObservableList<vainUser>  obsUserList = FXCollections.observableArrayList();
	private vainUser obsUserName;
	
	/**
	 * @author Logan Lautt
	 * The following function sets the userList and the primaryStage of the class for when it has to launch additional FXMLs
	 *
	 * @param	UserList	the main userlist of the program
	 * @param primaryStage	the primaryStage for launching FXMLs
	 */
	public void setStuff(userList UserList, Stage primaryStage) {
		
		this.UserList = UserList;
		this.primaryStage = primaryStage;
		refreshObsList();
	}
	
	/**
	 * Admin Sub-System constructor
	 */
	public adminSSController() {
		
	}
	
	/**
	 * Returns the observable list of usernames
	 * 
	 * @return	the observable list of usernames
	 */
    public ObservableList<vainUser> getObsUserList() {
        return obsUserList;
    }
	
	/**
	 * @author Logan
	 * Updates the ListView with the observable list of usernames 
	 */
	@FXML
    private void initialize(){
		
		refreshObsList();
		
		// Add observable list of usernames to the ListView
		listView.setItems(obsUserList);
		
		listView.setCellFactory(new Callback<ListView<vainUser>, ListCell<vainUser>>() {

            public ListCell<vainUser> call(ListView<vainUser> param) {
                ListCell<vainUser> cell = new ListCell<vainUser>() {

                    @Override
                    protected void updateItem(vainUser item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                        	textProperty().bind(item.getNameProperty());
                        }
                        else{
                        	textProperty().unbind();
                        	textProperty().set("");
                        }
                    }
                };
                return cell;
            }
        });
    }
	
	/**
	 * Updates the observable list of usernames
	 */
	void refreshObsList() {
		
		obsUserList.clear();
		System.out.println("\nUserList:");

		for (User user: UserList.users) {
		
			obsUserName = new vainUser(user.getName());
			obsUserList.add(obsUserName);
			
			System.out.println(user.getName());
		}
		
	}
	
	/**
	 * @author Logan
	 * Launches a dialog window that allows the admin to add a new User to the userList
	 */
	@FXML
	void addUser() {
		
		try {
			// Launch the user creation dialog
			FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(getClass().getResource("/viewtiful/NewUserDialog.fxml"));
	        AnchorPane foundation;
	        
			foundation = (AnchorPane)loader.load();
			
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("ADD NEW USER");
	        dialogStage.initOwner(primaryStage);
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        
	        Scene scene = new Scene(foundation);
	        dialogStage.setScene(scene);
	            
	        newUserController newser = loader.getController();
	        newser.setStuff(UserList, dialogStage);
	              
	        dialogStage.showAndWait();
	        
	        // Update the observable list of usernames
	        refreshObsList();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	/**
	 * @author Logan
	 * Deletes currently selected user, prompts admin for confirmation
	 */
	@FXML
	void deleteUser() {
    	
		// Get username of currently selected user
		vainUser victim = listView.getSelectionModel().getSelectedItem();
		
		if ((UserList.findUser(victim.getName())) && !(victim.getName().equals("admin"))) {
    		
    		Alert alert = new Alert(AlertType.CONFIRMATION);
    		alert.setTitle("DELETE USER");
    		alert.setHeaderText("Confirm User Deletion");
    		alert.setContentText("Are you sure you want to delete this user?");

    		Optional<ButtonType> result = alert.showAndWait();
    		if (result.get() == ButtonType.OK){
    			
    			UserList.users.removeIf(u -> (u.getName() == victim.getName()));    			
    		} 
    	}
		else if (victim.getName().equals("admin")) {
    		Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(primaryStage);
            alert.setTitle("NO ANARCHY FOR YOU");     
            alert.setContentText("DON'T DELETE THE ADMIN, WHAT'S WRONG WITH YOU");
            alert.showAndWait();
		}
		else{
    		Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(primaryStage);
            alert.setTitle("YOU DIDN'T SELECT ANYTHING DOOFUS");     
            alert.setContentText("SELECT SOMETHING ON THE TABLE, WHY DON'T YA");
            alert.showAndWait();
    	}
		
		refreshObsList();
		saveAndLoad.saveGame(UserList);
	}
	public void logout() throws IOException{
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/viewtiful/Login.fxml"));
        Pane loginRoot=(Pane)loader.load();
        
        loginController lcontroller=loader.getController();
        lcontroller.setStuff(UserList, primaryStage);
        
        
        Scene loginScene= new Scene(loginRoot);
        primaryStage.setScene(loginScene);
        primaryStage.show();
	}
}
