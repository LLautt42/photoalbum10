package puppeteer;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import maine.saveAndLoad;
import maine.userList;
import model.User;

public class newUserController {

	public userList UserList;
	public Stage primaryStage;
	
	@FXML Button Create;
	@FXML TextField Username;
	
	public void setStuff(userList UserList, Stage primaryStage){

		this.UserList=UserList;
		this.primaryStage=primaryStage;
	}
	
	public void startNew(){
		String temp=Username.getText();
		if(UserList.findUser(temp)){
			Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("YOU DIDN'T PUT AN EXTANT NAME IN, DOOFUS");
            alert.setContentText("ACCESS DENIED!");
            alert.showAndWait();
			
		}
		else{
			User newGuy=new User(temp);
			UserList.addUser(newGuy, UserList);
			saveAndLoad.saveGame(UserList);
		}
		
		primaryStage.close();
	}
}
