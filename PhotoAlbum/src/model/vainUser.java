package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Album class wrapper for listViews
 * 
 * @author Angeline
 *
 */
public class vainUser {
	
	StringProperty name;
	
	public vainUser (String name){
		this.name=new SimpleStringProperty(name);
	}
	
	public String getName(){
		return this.name.get();
	}
	public void setName(String name){
		this.name.set(name);
	}
	public StringProperty getNameProperty(){
		return name;
	}
}
