package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Tag class wrapper for TableViews
 * 
 * @author Angeline
 *
 */
public class vainTag {
	
	StringProperty type;
	StringProperty value;
	
	public vainTag (String type, String value){
		this.type=new SimpleStringProperty(type);
		this.value=new SimpleStringProperty(value);
	}
	
	public String getType(){
		return this.type.get();
	}
	public void setType(String type){
		this.type.set(type);
	}
	public StringProperty getTypeProperty(){
		return type;
	}
	
	public String getValue(){
		return this.value.get();
	}
	public void setValue(String value){
		this.value.set(value);
	}
	public StringProperty getValueProperty(){
		return value;
	}
}
