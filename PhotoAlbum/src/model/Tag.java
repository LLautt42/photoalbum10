package model;

public class Tag implements java.io.Serializable {

	private static final long serialVersionUID = -5475881651789475400L;
	private String tagType;
	private String tagValue;
	
	public Tag(String tagType, String tagValue) {
		this.tagType = tagType;
		this.tagValue = tagValue;
	}
	
	public String getType() {
		return tagType;
	}
	
	public void setType(String type) {
		tagType = type;
	}
	
	public String getValue() {
		return tagValue;
	}
	
	public void setValue(String value) {
		tagValue = value;
	}
}