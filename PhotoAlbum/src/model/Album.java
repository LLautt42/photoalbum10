package model;

import java.util.List;
import java.util.ArrayList;

/**
 * Album class implementation
 * 
 * Albums are identified by name and have photos.
 * When an album is opened, all of its photos are displayed (along with their thumbnails and captions),
 * and photos can be added, deleted, (re)captioned, 
 *  
 * @author Angeline Tamayo
 *
 */
public class Album implements java.io.Serializable {
	
	private static final long serialVersionUID = 522602950454895063L;
	private String albumName;
	private List<Photo> photos;
	
	/**
	 * Album constructor
	 * 
	 * @param albumName	the name of the album
	 */
	public Album(String albumName) {
		photos = new ArrayList<Photo>();
		this.albumName = albumName;
	}
	
	/**
	 * Gets the album's name.
	 * 
	 * @return	the name of the album
	 */
	public String getName() {
		return albumName;
	}
	
	/**
	 * Sets the album's name.
	 * 
	 * @param name	the desired name of the album
	 */
	public void setName(String name) {
		albumName = name;
	}
	
	/**
	 * Adds a photo to the album.
	 * 
	 * @param photo	the desired photo to be added
	 */
	public void addPhoto(Photo photo) {
		
		photos.add(photo);
	}
	
	/**
	 * Deletes a photo from the album.
	 * 
	 * @param photoName	the name of the photo to be deleted
	 */
	public void deletePhoto(String photoName) {
		for (Photo photo : photos) { // iterate through photos in album
			if (photo.getName().equals(photoName)) {
				photos.remove(photo);
				break;
			}
		}
	}
	
	/**
	 * Gets all of the photos in the album.
	 * 
	 * @return	a list of all of the photos
	 */
	public List<Photo> getPhotos() {
		ArrayList<Photo> albumPhotos = new ArrayList<Photo>(photos);
		return albumPhotos;
	}
}