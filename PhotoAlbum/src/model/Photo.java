package model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.io.File;

/**
 * Photo class implementation
 * 
 * Photos are identified by name and have captions (which can be edited),
 * a capture date and time, and tags (which can be added or deleted).
 * 
 * @author Angeline Tamayo
 *
 */
public class Photo implements java.io.Serializable {

	private static final long serialVersionUID = -2541141789759605876L;
	private String photoName;
	private String photoCaption;
	private Calendar photoDateTime;
	private List<Tag> tags;
	
	
	/**
	 * Photo constructor
	 * 
	 * @param photoName		the name of the photo
	 * @param photoCaption	the caption associated with the photo
	 */
	public Photo(String photoName, String photoCaption) {
		File fp = new File(photoName);
		this.photoName = photoName;
		this.photoCaption = photoCaption;
		photoDateTime = Calendar.getInstance();
		photoDateTime.setTimeInMillis(fp.lastModified());
		photoDateTime.set(Calendar.MILLISECOND, 0);
		tags = new ArrayList<Tag>();
	}
	
	/**
	 * Gets the name of the photo.
	 * 
	 * @return	the name of the photo
	 */
	public String getName() {
		return photoName;
	}
	
	/**
	 * Sets the name of the photo.
	 * 
	 * @param fileName	the name of the photo
	 */
	public void setName(String fileName) {
		photoName = fileName;
	}
	
	/**
	 * Gets the caption of the photo.
	 * 
	 * @return the caption of the photo
	 */
	public String getCaption() {
		return photoCaption;
	}
	
	/**
	 * Sets the caption of the photo.
	 * 
	 * @param caption the desired caption of the photo
	 */
	public void setCaption(String caption) {
		photoCaption = caption;
	}
	
	/**
	 * Gets the capture date and time of the photo.
	 * 
	 * @return	the capture date and time
	 */
	public Calendar getDateTime() {
		return photoDateTime;
	}
	
	/**
	 * Sets the capture date and time of the photo.
	 * 
	 * @param dateTime	the desired capture date and time
	 */
	public void setDateTime(Calendar dateTime) {
		photoDateTime = dateTime;
		photoDateTime.set(Calendar.MILLISECOND, 0);
	}
	
	/**
	 * Adds a tag (type+value pair) to the photo.
	 * 
	 * @param type	the type of the tag (e.g. "location")
	 * @param value	the value of the tag (e.g. "New Brunswick")
	 */
	public void addTag(String type, String value) {
		Tag tag = new Tag(type, value);
		tags.add(tag);
	}
	
	/**
	 * Deletes a tag.
	 * 
	 * @param type	the type of the tag
	 * @param value	the value of the tag
	 */
	public void deleteTag(String type, String value) {
		tags.removeIf(t -> (t.getType().equals(type) && t.getValue().equals(value)));
	}
	
	/**
	 * Gets all of the tags of the photo.
	 * 
	 * @return	all of the tags associated with the photo
	 */
	public List<Tag> getTags() {
		ArrayList<Tag> photoTags = new ArrayList<Tag>(tags);
		return photoTags;
	}	
}