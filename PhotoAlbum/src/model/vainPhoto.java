package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class vainPhoto {
	
	StringProperty name;
	StringProperty caption;
	
	public vainPhoto (String name){
		this.name = new SimpleStringProperty(name);
		caption = new SimpleStringProperty(" ");
	}
	
	public String getName(){
		return this.name.get();
	}
	public void setName(String name){
		this.name.set(name);
	}
	public StringProperty getNameProperty(){
		return name;
	}
	
	public String getCaption(){
		return this.caption.get();
	}
	
	public void setCaption(String caption){
		this.caption.set(caption);
	}
	
	public StringProperty getCaptionProperty() {
		return caption;
	}

}
