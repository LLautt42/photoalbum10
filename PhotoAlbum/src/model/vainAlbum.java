package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class vainAlbum {
	
	StringProperty name;
	
	public vainAlbum (String name){
		this.name=new SimpleStringProperty(name);
	}
	
	public String getName(){
		return this.name.get();
	}
	public void setName(String name){
		this.name.set(name);
	}
	public StringProperty getNameProperty(){
		return name;
	}

}
