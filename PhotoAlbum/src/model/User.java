package model;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

/**
 * User class implementation
 * 
 * Users are identified by username and have albums (which they can create, delete,
 * or rename) and photos (which they can add to or delete from albums).
 * 
 * @author Angeline Tamayo
 *
 */

public class User implements java.io.Serializable {

	private static final long serialVersionUID = -4206385201337654103L;
	private String userName;
	private Map<String, Photo> photos;
	private Map<String, Album> albums;
	
	/**
	 * User constructor
	 * 
	 * @param userName	the username of the user
	 */
	public User(String userName) {
		this.userName = userName;
		this.photos = new HashMap<String, Photo>();
		this.albums = new HashMap<String, Album>();
	}
	
	/**
	 * Gets the user's username.
	 * 
	 * @return	the username of the user
	 */
	public String getName() {
		return userName;
	}
	
	/**
	 * Sets the user's username.
	 * 
	 * @param	name the desired username of the user
	 */
	public void setName(String name) {
		userName = name;
	}
	
	/**
	 * Gets an album specified by its name.
	 * 
	 * @param name	the name of the album
	 * @return		the desired album
	 */
	public Album getAlbum(String name) {
		//name = name.toLowerCase();
		return albums.get(name);
	}
	
	
	/**
	 * Gets all of the user's albums.
	 * 
	 * @return	the list of the user's albums
	 */
	public List<Album> getAllAlbums() {
		List<Album> albums = new ArrayList<Album>(this.albums.values());
		return albums;
	}
	
	/**
	 * Creates a new album.
	 * 
	 * @param albumName	the desired name of the album to be created
	 */
	public void addAlbum(String albumName) {
		Album album = new Album(albumName);
		albums.put(albumName, album);
	}
	
	/**
	 * Deletes an album specified by its name.
	 * 
	 * @param albumName	the name of the album to be deleted
	 */
	public void deleteAlbum(String albumName) {
		albums.remove(albumName);
	}
	
	/**
	 * Renames an album.
	 * 
	 * @param oldName	the current name of the album to be renamed
	 * @param newName	the desired new name of the album
	 */
	public void renameAlbum(String oldName, String newName) {
		Album album = albums.get(oldName);
		album.setName(newName);
		albums.put(newName, album);
		albums.remove(oldName);
	}	
	
	/**
	 * Adds a photo to the user's library of photos.
	 * 
	 * @param photo	the photo to be added
	 */
	public void addPhoto(Photo photo) {
		photos.put(photo.getName(), photo);
	}
	
	/**
	 * Deletes a photo from the user's library of photos.
	 * 
	 * @param photoName	the name of the photo to be deleted
	 */
	public void deletePhoto(String photoName) {
		photos.remove(photoName);
	}
	
	/**
	 * Gets a photo specified by its name from the user's library of photos.
	 * 
	 * @param name	the name of the photo
	 * @return		the desired photo
	 */
	public Photo getPhoto(String name) {
		return photos.get(name);
	}
	
	/**
	 * Gets all of the user's photos.
	 * 
	 * @return the list of the user's photos
	 */
	public List<Photo> getAllPhotos() {
		List<Photo> photoList = new ArrayList<Photo>(photos.values());
		return photoList;
	}
}
