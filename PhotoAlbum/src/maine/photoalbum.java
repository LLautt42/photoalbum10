package maine;

import java.io.IOException;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import maine.userList;
import puppeteer.loginController;

/**
 * @author Logan Lautt
 * The following class is the main class of the program, and where the login, or root, fxml is launched.  This also loads an existing fxml,
 * if there is one.
 * 
 * @param	primaryStage	The primary stage to launch FXMLs
 * @param	userList		A list of all extant users currently in the program.
 *
 * @extends Application
 */
public class photoalbum extends Application {
	
	public static Stage primaryStage;
	public static userList uList;
	
	public photoalbum(){}
	
	/**
	 * @author Logan Lautt
	 * The following function is the basic start function for the entire program, launching the first fxml login screen.
	 *
	 *@param	primaryStage	the primary Stage of the programing for launching all FXMLs
	 * @throws IOException
	 */
	@Override
	public void start(Stage primaryStage) throws IOException{
		 this.primaryStage = primaryStage;
	     this.primaryStage.setTitle("Picture Library");
	     commence(primaryStage);
	}
	
	public void setToAdminController(userList uList) throws IOException{
		
	}
	public void setToAlbumListController(userList uList){
		
	}
	public void setToDeleteController(userList uList){
		
	}
	public void setToLoginController(userList uList){
		
	}
	public void setToMovePhotoController(){
		
	}
	public void setToNewElementController(){
		
	}
	public void setToPhotoListController(){
		
	}
	public void setToRenameAlbumController(){
		
	}
	public void setToSearchPhotoController(){
		
	}
	
	/**
	 * @author Logan Lautt
	 * The following function starts the login screen.
	 * 
	 * @param	primaryStage	The primary Stage of the program, used to launch FXMLs
	 * @throws IOException
	 */
	public void commence(Stage primaryStage) throws IOException{
		 FXMLLoader loader = new FXMLLoader();
         loader.setLocation(getClass().getResource("/viewtiful/Login.fxml"));
         Pane loginRoot=(Pane)loader.load();
         
         loginController lcontroller=loader.getController();
         lcontroller.setStuff(uList, primaryStage);
         
         
         Scene loginScene= new Scene(loginRoot);
         primaryStage.setScene(loginScene);
         primaryStage.show();
         
         
         //Begin here;
	}
	
	/**
	 * @author Logan Lautt
	 * The following function returns the current primaryStage
	 *
	 * @return	the primary stage of the program
	 */
	
	public Stage getPrimaryStage(){
		return primaryStage;
	}
	
	public static void main (String[] args){
		uList=maine.saveAndLoad.loadGame();
		maine.saveAndLoad.saveGame(uList);
		launch(args);
	}

}
