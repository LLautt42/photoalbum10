package maine;
import java.util.ArrayList;
import java.util.List;

import model.Photo;
import model.User;

/**
 * @author Logan Lautt
 * The following is the main list of users in the photoalbum.
 * 
 * @param users		the userlist that contains all users in the current session.
 */

public class userList implements java.io.Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 984478704875444987L;
	public List<User> users;
	
	public userList(){
		this.users= new ArrayList<User>();
	}
	
	/**
	 * @author Logan Lautt
	 *Initializes a new user list that just has the administrator as the lone user.
	 * 
	 * @return		the new userlist
	 */
	
	//Spawns the list and adds the administrator
	
	public static userList initUserList(){
		userList listo=new userList();
		User admin=new User("admin");
		listo.users.add(admin);
		return listo;
	}
	
	/**
	 * @author Logan Lautt
	 * Finds a specific user in the userList.
	 * 
	 * @param name		the name of the user to be searched
	 * @return		true if the name is in the list, false otherwise.
	 */
	public boolean findUser(String name){
		//Scrolls through all users in list
		for(User user: users){
			if((user.getName()).equals(name)){
				return true;
			}
		}
		
		
		return false;
	}
	public User findUserName(String name){
		//Scrolls through all users in list
		for(User user: users){
			if((user.getName()).equals(name)){
				return user;
			}
		}
		
		
		return null;
	}
	
	public void addUser(User user, userList listo){
		listo.users.add(user);
		return;
	}
	public boolean removeUser(User user, userList listo){
		if(listo.users.remove(user)){
			return true;
		}
		return false;
	}

}
