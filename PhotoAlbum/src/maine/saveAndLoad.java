package maine;
import java.io.*;


import model.Album;
import model.Photo;
import model.User;

public class saveAndLoad {
	
	
	/**
	 * @author Logan Lautt
	 * Saves the state of the current userList to the serializable file to be loaded when the program exits.
	 * 
	 * @param u		the userlist to be saved into the serializaable file
	 * @return		true if the file was saved successfully, and false on an error.
	 */
	public static boolean saveGame(userList u){
		
		try
	      {
	         FileOutputStream fileOut = new FileOutputStream("src/data/listo.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(u);
	         out.close();
	         fileOut.close();
	      }
		  catch(IOException i)
	      {
	          i.printStackTrace();
	          return false;
	      }
		return true;
	}

	/**
	 * @author Logan Lautt
	 * Loads the serializable file into the program to resume the previous state of the program.
	 * 
	 * 
	 * @return		the userlist from the serializable file.  If no such file exists, the program then makes a new file
	 * 				with just the admin as a user.
	 */
	public static userList loadGame(){
		userList u=null;
		try{
			FileInputStream filo = new FileInputStream("src/data/listo.ser");
	        ObjectInputStream in = new ObjectInputStream(filo);
	        u = (userList) in.readObject();
	        in.close();
	        filo.close();
		}
		
		catch(FileNotFoundException i)
	    {
			System.out.println("userList file not found");
			userList us=userList.initUserList();
		    return us;
	    }
		
		catch(ClassNotFoundException c)
	    {
			System.out.println("userList class not found");
			userList us=userList.initUserList();
		    return us;
	    } 
		catch (IOException e) {
			System.out.println("something went wrong");
	    	userList us=userList.initUserList();
		    return us;
		}
		
		return u;
	}
	
	
}
